export class Quiz {
  public id: number;
  public task_id: number;
  public answer: string;
  public option_a: string;
  public option_b: string;
  public option_c: string;
  public option_d: string;
  public question: string;

  constructor() {
    this.id = null;
    this.task_id = null;
    this.answer = '';
    this.option_a = '';
    this.option_b = '';
    this.option_c = '';
    this.option_d = '';
    this.question = '';
  }
}