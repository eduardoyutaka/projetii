export class Project {
  public id: number;
  public detail_long: string;
  public detail_short: string;
  public estimated_time: number;
  public image: string;
  public grade: string;
  public price: number;
  public task_number: number;
  public title: string;
  public thumbnail: string

  constructor() {
    this.id = null;
    this.detail_long = '';
    this.detail_short = '';
    this.estimated_time = null;
    this.image = '';
    this.grade = '';
    this.price = null;
    this.task_number = null;
    this.title = '';
    this.thumbnail = '';
  }
}