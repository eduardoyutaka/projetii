export class Task {
  public id: number;
  public project_id: number;
  public task_id: number;
  public user_id: number;
  public done: boolean;

  constructor() {
    this.id = null;
    this.project_id = null;
    this.task_id = null;
    this.user_id = null;
    this.done = false;
  }
}