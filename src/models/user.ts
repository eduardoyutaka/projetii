export class User {
  public id: number;
  public api_key: string;
  public email: string;
  public password: string;

  constructor() {
    this.id = null;
    this.api_key = '';
    this.email = '';
    this.password = '';
  }
}