import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers, RequestOptions} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Project } from '../models/project';
import { Task } from '../models/task';
import { User } from '../models/user';


@Injectable()
export class ApiService {
  private ip: string;
  private version: string;
  private postSignupUrl: string;
  private postLoginUrl: string;
  private indexProjectUrl: string;
  private indexTaskUrl: string;
  private showProjectUrl: string;
  private showTaskUrl: string;
  private showQuizUrl: string;
  private buyProjectUrl: string;

  constructor(private http: Http) {
    this.ip = 'http://13.59.135.16:4000/'
    this.version = 'api/v1/';
  }

  postSignup(user: User): Observable<any> {
    let bodyString = JSON.stringify(user); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});

    this.postSignupUrl = this.ip+'user';

    return this.http.post(this.postSignupUrl, bodyString, options)
      .map(res => res.json());
  }

  postLogin(user: User): Observable<any> {
    let bodyString = JSON.stringify(user); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});

    this.postLoginUrl = this.ip+'login';

    return this.http.post(this.postLoginUrl, bodyString, options)
      .map(res => res.json());
  }

  getIndexProject(): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.indexProjectUrl = this.ip+this.version+'project';

    return this.http.get(this.indexProjectUrl, options)
      .map(res => res.json());
  }

  getIndexTask(project: Project): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.indexTaskUrl = this.ip+this.version+'task/project/'+project.id;

    return this.http.get(this.indexTaskUrl, options)
      .map(res => res.json());
  }

  getShowProject(id: number): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.showProjectUrl = this.ip+this.version+'project/'+id;

    return this.http.get(this.showProjectUrl, options)
      .map(res => res.json());
  }

  getShowTask(id: number): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.showTaskUrl = this.ip+this.version+'project/'+id;

    return this.http.get(this.showTaskUrl, options)
      .map(res => res.json());
  }

  getShowQuiz(project: Project, task: Task): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.showQuizUrl = this.ip+this.version+'quiz/task/'+task.id;

    return this.http.get(this.showQuizUrl, options)
      .map(res => res.json());
  }
    
  getBuyProject(project: Project): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers});
    headers.append('Authorization', 'Token token='+localStorage.getItem('key'));

    this.buyProjectUrl = this.ip+this.version+'project/'+project.id+'/buy';

    return this.http.get(this.buyProjectUrl, options)
      .map(res => res.json());
  }
}
