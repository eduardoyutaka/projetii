import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ApiService } from '../../services/api.service';

import { Project } from '../../models/project';
import { Task } from '../../models/task';

import { ProjectsPage } from '../projects/projects';
import { TaskPage } from '../task/task';
import { GalleryPage } from '../gallery/gallery';

@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html'
})
export class TasksPage implements OnInit {
  public project: Project;
  public tasks: Array<Task>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public _apiService: ApiService) {
    this.project = this.navParams.get('project');
    this.tasks = new Array(new Task());
  }

  ngOnInit() {
    this._apiService.getIndexTask(this.project)
      .subscribe(tasks => {
        this.tasks = tasks;
      }, err => {
        console.log(err);
      });
  }

  pushProjectsPage() {
    this.navCtrl.push(ProjectsPage);
  }

  pushTaskPage(task: Task) {
    this.navCtrl.push(TaskPage, {
      project: this.project,
      task: task
    });
  }

  pushGalleryPage() {
    this.navCtrl.push(GalleryPage);
  }
}
