import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

import { ApiService } from '../../services/api.service';

import { User } from '../../models/user';

import { ProjectsPage } from '../projects/projects';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public user: User;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public _apiService: ApiService) {
    this.user = new User();
  }

  login() {
    this._apiService.postLogin(this.user)
      .subscribe(user => {
        this.user = user;
        localStorage.setItem('key', this.user.api_key);
        this.navCtrl.setRoot(ProjectsPage);
      }, err => {
        this.showAlert(err.json().msg);
     });
  }

  showAlert(errMsg: string) {
    let alert = this.alertCtrl.create({
      title: 'Oops!',
      subTitle: errMsg,
      buttons: ['OK']
    });
    alert.present();
  }
}
