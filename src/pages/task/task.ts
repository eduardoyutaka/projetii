import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Project } from '../../models/project';
import { Task } from '../../models/task';

import { QuizPage } from '../quiz/quiz';
import { GalleryPage } from '../gallery/gallery';


@Component({
  selector: 'page-task',
  templateUrl: 'task.html'
})
export class TaskPage {
  public project: Project;
  public task: Task;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.project = this.navParams.get('project');
    this.task = this.navParams.get('task');
    console.log(this.task);
  }

  pushQuizPage() {
    this.navCtrl.push(QuizPage, {
      project: this.project,
      task: this.task
    });
  }

  pushGalleryPage() {
    this.navCtrl.push(GalleryPage);
  }
}
