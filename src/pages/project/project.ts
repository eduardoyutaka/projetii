import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Project } from '../../models/project';

import { TasksPage } from '../tasks/tasks';

@Component({
  selector: 'page-project',
  templateUrl: 'project.html'
})
export class ProjectPage {
  public project: Project

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.project = this.navParams.get('project');
  }

  setRootTasksPage() {
  	this.navCtrl.setRoot(TasksPage, {
      project: this.project
    });
  }
}
