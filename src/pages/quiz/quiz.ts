import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { ApiService } from '../../services/api.service';

import { Project } from '../../models/project';
import { Quiz } from '../../models/quiz';
import { Task } from '../../models/task';

import { TasksPage } from '../tasks/tasks';

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html'
})
export class QuizPage implements OnInit {
  public project: Project;
  public quiz: Quiz;
  public task: Task;
  public answerA: string;
  public answerB: string;
  public answerC: string;
  public answerD: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _apiService: ApiService) {
    this.project = this.navParams.get('project');
    this.quiz = new Quiz();
    this.task = this.navParams.get('task');
    this.answerA = '';
    this.answerB = '';
    this.answerC = '';
    this.answerD = '';
    console.log(this.project);
    console.log(this.task);
  }

  ngOnInit() {
    this._apiService.getShowQuiz(this.project, this.task)
      .subscribe(quiz => {
        this.quiz = quiz;
        console.log(this.quiz);
      }, err => {
        console.log(err);
      });
  }

  setAnswer(option: string) {
    
  }

  setRootTasksPage() {
    this.navCtrl.setRoot(TasksPage, {
      project: this.project
    });
  }
}
