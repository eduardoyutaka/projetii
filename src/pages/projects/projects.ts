import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ApiService } from '../../services/api.service';

import { Project } from '../../models/project';

import { ProjectPage } from '../project/project';

@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html'
})
export class ProjectsPage implements OnInit {
  public projects: Array<Project>;

  constructor(public navCtrl: NavController, private _apiService: ApiService) {
    this.projects = new Array(new Project());
  }

  ngOnInit() {
    this._apiService.getIndexProject()
      .subscribe(projects => {
        this.projects = projects;
      }, err => {
        console.log(err);
      });
  }

  pushProjectPage(project: Project) {
    this.navCtrl.push(ProjectPage, {
      project: project
    });
  }
}
